# Publishing Images

There's not much point in our images living on our local machines, right? We need to get these into the "cloud" so that we can be "web-scale". Docker provides the Docker Hub, where you can push all your public images.

###### Warning: Anybody can pull your public images and take whatever is inside.

In order to push your images to the Docker Hub, or any other registry, you need to follow the format:

`repository_name/project_name:version`

### Examples

```shell
$ docker build -t rawkode/my_new_image:v2 .
# You can also tag an image after build: docker tag current_tag|id new_tag:version
$ docker tag my_new_image:latest rawkode/my_new_image:v2
```

## Private Image Repository Options

- Docker Hub allows you to pay a nominal fee for private repositories
- GitLab provides a free Docker Registry

## Private Images, DIY Style

We have to remember that Docker Images are just snapshots of a file-system. You can actually flatten the lawyers and save your Docker Image as a `tar`. Once it's a tar ... you can store it in Amazon S3, or a SAN/NFS/ANOther. The "cloud" is your oyster.

### Saving a Docker Image to a Tar

```shell
$ docker save -o rawkode_image_v2.tar rawkode/my_new_image:v2
```

### Loading a Docker Image from a Tar

```shell
$ docker load -i rawkode_image_v2.tar
```

